### HistPlotter.py
# Purpose:  This script creates plots which contain multiple histograms
#           created from pxl NVectrors. These vectors can for example be 
#           created with the TextFileParser Generator.
#           
# Author:   Joschka Lingemann
# Created:  Tue Jul 20 10:56:16 2010


# Import all necessary stuff from root:
from ROOT import gROOT, gStyle, TH1F, TCanvas, THStack
# This we'll need for simple platform independent
# manipulation of paths:
from os import path
# PXL stuff
from pxl import modules

class HistStackPlotter(object):
    def __init__(self):
        '''
            Initialize the class, initialize private variables
        '''
        self._moduleRef = None

        self._variableTuple = []
        self._units = []
        self._titles = []
        self._saveAs = ""
        self._optionsGood = True
        pass

    def initialize(self, module):
        """
            This is executed when you add the module or reload it.
            It adds the otions in the VISPA GUI
        """

        print "--- Init: HistStackPlotter"

        # Here the options one can access via the VISPA GUI are added:

        # In the variableTuples option one can set which variables shall be drawn. 
        # If in the text file parser the option "titles" was enabled this is 
        # quite straight forward: Just add the variable-titles separated ',' for 
        # variables in the same plot, called tuples and separate tuples by spaces:
        # E.g. "x,y y,z"
        module.addOption("variableTuples", "Variable tuple = variables in one plot to plot, seperated by commas, tuples seperated by space. Stack will be in same order as noted here.", "x,y,z E',E")

        # Units have to be filled into the "units" field in the same order as the 
        # variable-tuples. Only one unit per variable tuple!
        # If a variable has no unit add a 0 for this variable.
        # The unit for a variable will be added as an appendix to the axis 
        # title, e.g. if variables is "x,y y,z" and titles "d s" and units is "0 cm" 
        # it will result in the x-axis being titled "d" for the first plot and "s (cm)" 
        # for the second plot
        module.addOption("units", "Units have to be denoted per variable tuple", "cm GeV")

        # Since you plot more than one variable which have different titles this cannot be
        # set automatically anymore. Specify one title per tuple.
        module.addOption("titles", "Titles have to be given per variable tuple", "x E")

        # The saveAs option is for the output of this module.
        # Graphs will be saved in the given format and with the
        # given title enumerated in the order as they were put 
        # in the variable pair field.
        # E.g. the saveAs option graphPlot.pdf will result in 
        # plots being saved as pdfs with the titles graphPlot0.pdf,
        # graphPlot1.pdf, etc.
        module.addOption("saveAs", "Specify a format in which the plot should be saved. Possible filetypes: pdf, eps, ps, png, gif, jpg", "stackPlot.pdf", modules.OptionDescription().USAGE_FILE_SAVE)

        # Set this for later access of the options
        self._moduleRef = module

    def beginJob(self, parameters=None):
        '''
            Here all options are get and dumped in corresponding variables.
            Only edit this, if you know what you are doing.
        '''

        print '*** Begin job: HistStackPlotter'

        # If module is misconfigured this will be set to false and the 
        # module will make no plots.
        self._optionsGood = True

        # Get the variable-tuples. variableTuplesRaw contains now the tuples which
        # are still separated by ','
        variableTuplesRaw = self._moduleRef.getOption("variableTuples").split()
        # if no variables are specified...
        if not variableTuplesRaw:
            print "ERROR in HistStackPlotter: Please specify variables to plot."
            self._optionsGood = False
        # Now divide the tuples into the variable names and put them in a list
        for item in variableTuplesRaw:
            tuple = item.split(",")
            self._variableTuple.append(tuple)

        # Get the units which are divided by spaces and put them in a list
        self._units = self._moduleRef.getOption("units").split()
        # if more or less units than tuples:
        if len(self._units) != len(variableTuplesRaw):
            print "WARNING in HistStackPlotter: Please specify as many units as variable-tuple and only one unit per tuple."

        # Get the titles which are divided by spaces and put them in a list        
        self._titles = self._moduleRef.getOption("titles").split()
        # if more or less units than tuples:
        if len(self._titles) != len(variableTuplesRaw):
            if len(self._titles) < len(variableTuplesRaw):
                raise NameError("ERROR in HistStackPlotter: Please specify a title per variable-tuple and only one title per tuple.")
            else:
                print "WARNING in HistStackPlotter: Please specify as many titles as variable-tuple and only one title per tuple."

        # Get the saveAs string in a form so we can append numbers
        # for multiple plots per module instance...
        saveAsRaw = self._moduleRef.getOption("saveAs")
        if not saveAsRaw:
            raise NameError("ERROR in HistPlotter: Please specify the saveAs option!")
        # pathHead will now contain the path to the folder where
        # we want to save, pathTail only the filename:
        pathHead, pathTail = path.split(saveAsRaw)
        # This loop takes care of filenames with more than one 
        # . in them, for example my.plot.pdf 
        for i, pathTailPart in enumerate(pathTail.split(".")):
            if i == 0:
                self._saveAs = path.join(pathHead, pathTailPart)
            elif i < len(pathTail.split(".")) - 1:
                self._saveAs += pathTailPart
            else:
                self._saveAs += "{filenumber}." + pathTailPart

    def analyse(self, object):
        '''
            Executed on every object:
            Think of this as part of your for-loop which would go over all
            the entries in your text file.
            The actual work is being done here. 
            
            Please ONLY EDIT the code outside the user hooks 
            only IF YOU KNOW WHAT YOU'RE DOING.
        '''

        # The NVectors are saved in so called containers, this converts the object
        #  which is handed from module to module into this original container. 
        container = core.toBasicContainer(object)
        fileCounter = 0

        # This will setup a standard style for all plots and the canvases on which
        # the plots are drawn.
        # Customization will be possible anyway, using the normal pyROOT syntax
        self.setupRoot()

        # If the module is not correctly configured skip the processing to 
        # prevent crashes
        if not self._optionsGood:
            pass

        # This for-loop searches for the variables in the event created
        # by the parser and writes them into arrays which will be used
        # to create the graphs later.
        for i, variableNames in enumerate(self._variableTuple):
            # variableNames now contains a list of all variable-titles
            # which shall be displayed in one plot.

            # First lets get the unit of the variable, if one is given:
            # Initialize the string which will be appended to the x-axis
            # title
            unitString = ""
            # If a unit exists for the current variable-tuple
            if len(self._units) > i:
                # and it is not 0
                if self._units[i] != "0":
                    # create the string. If you want to customize, just edit 
                    # this string.
                    unitString = " (" + self._units[i] + ")"
            # And also get the title for the x-axis:
            titleString = ""
            if len(self._titles) > i:
                titleString = self._titles[i]
            xTitle = titleString + unitString


            minimum = 0.
            maximum = 0.
            stack = THStack("stack" + str(i), "")
            for variableName in variableNames:
                found = False
                for variableSet in container.getObjects():
                    if variableSet.getName() == variableName:
                        found = True
                        # This will run over all values and evaluate which are the
                        # minimal and maximal values for each variable set and add
                        # some padding for histograms to be displayed nicely
                        # Minimum and maximum are given to the function so it 
                        # determines the min and max for all variables in one stack
                        minimum, maximum = self.determineExtrema(variableSet, minimum, maximum)
                if not found:
                    raise NameError("ERROR in HistPlotter: Sorry, could not find all variables specified make sure they are in the text file: could not find '" + variableName + "'")

            # Have to run twice over all because stacked plots have to have same range!
            for j, variableName in enumerate(variableNames):
                for variableSet in container.getObjects():
                    if variableSet.getName() == variableName:
### USER HOOK: Manipulate your histogram stacks here
                        # This initializes a new histogram and takes care of the looks 
                        # of the histogram, can be customized with normal pyRoot 
                        # syntax later.
                        # For color codes see: http://root.cern.ch/root/html/MACRO_TColor_1_c.gif
                        hist = TH1F ("h_" + variableSet.getName() + str(i) , "Line Entries", 100, minimum, maximum)
                        self.initHistStyle(hist=hist, color=j + 1, filled=True)

                        # If you want to change settings only for one variable tuple add if statement here.
                        # See for example the HistPlotter.py

                        #This fills the histogram with the content of a row or line...
                        for i in range(0, variableSet.getSize()):
                            hist.Fill(variableSet.getElement(i))

                        stack.Add(hist)
            c1 = TCanvas()
            stack.Draw()
            # Due to the nature of THStack the style must be set after drawing
            self.initStackStyle(stack=stack, xTitle=xTitle, yTitle="n")

            #find the correct output path
            self._saveAs = self._saveAs.format(filenumber=fileCounter)
            if (self._saveAs.find("\\") == -1 and  self._saveAs.find("/") == -1):
                self._saveAs = self._moduleRef.getAnalysis().getOutputPath() + self._saveAs

            c1.Print(self._saveAs)
            fileCounter += 1
            c1.WaitPrimitive()

### END USER HOOK


    def endJob(self):
        '''Executed after the last object'''
        print '*** End job: HistStackPlot'

    def determineExtrema(self, variableSet, minimum, maximum):
        for i in range(0, variableSet.getSize()):
            if variableSet.getElement(i) < minimum:
                minimum = variableSet.getElement(i)
            if variableSet.getElement(i) > maximum:
                maximum = variableSet.getElement(i)

        if minimum < 0.:
            minimum = minimum + 0.02 * minimum
        maximum = maximum + 0.02 * maximum
        return minimum, maximum


    def initStackStyle(self, stack, xTitle, yTitle, stackTitle=""):
        stack.GetXaxis().SetTitle(xTitle)
        stack.GetYaxis().SetTitle(yTitle)
        stack.SetTitle(stackTitle)

    def initHistStyle(self, hist, filled=False, color=1):
        hist.UseCurrentStyle()
        hist.SetLineColor(color)
        if filled:
            hist.SetFillColor(color)
            hist.SetLineColor(1)

    def setupRoot(self):
        gROOT.Reset()   # reset global variables

        gROOT.SetStyle("Plain")
        gStyle.SetOptStat(0)
        gStyle.SetOptFit(11)
        gStyle.SetStatX(0.9)
        gStyle.SetStatY(0.9)
        gStyle.SetStatW(0.3)
        gStyle.SetStatH(0.2)
        gStyle.SetNdivisions(505, "X")
        gStyle.SetNdivisions(505, "Y")
        gStyle.SetLabelOffset(0.008, "X")
        gStyle.SetLabelSize(0.08, "X")
        gStyle.SetTitleSize(0.08, "X")
        gStyle.SetTitleOffset(1.0, "X")
        gStyle.SetLabelOffset(0.02, "Y")
        gStyle.SetLabelSize(0.08, "Y")
        gStyle.SetTitleSize(0.08, "Y")
        gStyle.SetTitleOffset(1.4, "Y")
        gStyle.SetPadTopMargin(0.15)
        gStyle.SetPadBottomMargin(0.2)
        gStyle.SetPadLeftMargin(0.22)
        gStyle.SetPadRightMargin(0.1)
        gStyle.SetLineWidth(1)
        gStyle.SetMarkerStyle(20)
        gStyle.SetMarkerSize(0.9)
        gStyle.SetMarkerColor(2)
        gStyle.SetCanvasColor(10)
        gStyle.SetFrameFillColor(10)
