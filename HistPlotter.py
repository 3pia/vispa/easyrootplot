### HistPlotter.py
# Purpose:  This script creates plots which contain a histogram created
#           from pxl NVectrors. These vectors can for example be 
#           created with the TextFileParser Generator.
#           
# Author:   Joschka Lingemann
# Created:  Tue Jul 20 10:56:16 2010


# Import all necessary stuff from root:
from ROOT import gROOT, gStyle, TH1F, TCanvas
# This we'll need for simple platform independent
# manipulation of paths:
from os import path
# PXL stuff
from pxl import modules

class HistPlotter(object):
    def __init__(self):
        self._moduleRef = None

        self._variables = []
        self._units = []
        self._saveAs = ""

        self._optionsGood = True
        pass

    def initialize(self, module):
        """
            This is executed when you add the module or reload it.
            It adds the otions in the VISPA GUI
        """

        print "--- Init: HistPlotter"

        # Here the options one can access via the VISPA GUI are added:

        # In the variables option one can set which variables shall be drawn. 
        # If in the text file parser the option "titles" was enabled this is 
        # quite straight forward: Just add the variable-titles separated by 
        # spaces, e.g. "x y"
        module.addOption("variables", "Variables to plot, seperated by space. User Title (if activated) or line number (if titles deactivated)", "x y z")

        # Units have to be filled into the "units" field in the same order as the 
        # variables. 
        # If a variable has no unit add a 0 for this variable.
        # The unit for a variable will be added as an appendix to the axis 
        # title, e.g. if variables is "x y" and units is "0 cm" it will result 
        # in the x-axis being titled "x" for the first plot and "y (cm)" for the
        # second plot
        module.addOption("units", "Units of the plotted variables, seperated by space. In same order as variables above", "cm dm GeV")

        # The saveAs option is for the output of this module.
        # Graphs will be saved in the given format and with the
        # given title enumerated in the order as they were put 
        # in the variable pair field.
        # E.g. the saveAs option graphPlot.pdf will result in 
        # plots being saved as pdfs with the titles graphPlot0.pdf,
        # graphPlot1.pdf, etc.
        module.addOption("saveAs", "Specify how the plot should be saved. Name will be competed with numbers for multiple plots. Possible filetypes: pdf, eps, ps, png, gif, jpg", "plot.pdf", modules.OptionDescription().USAGE_FILE_SAVE)

        # Set this for later access of the options
        self._moduleRef = module

    def beginJob(self, parameters=None):
        '''
            Here all options are get and dumped in corresponding variables.
            Only edit this, if you know what you are doing.
        '''
        print '*** Begin job: HistPlotter'

        # If module is misconfigured this will be set to false and the 
        # module will make no plots.
        self._optionsGood = True

        # Add the variables into a list. The variables have to be 
        # separated by spaces in the option.
        self._variables = self._moduleRef.getOption("variables").split()
        if not self._variables:
            print "ERROR in HistPlotter: Please specify variables to plot."
            self._optionsGood = False


        # Get the units and fill them in a list. They will be in the same
        # order as they are written in the option, so they should be in
        # same order as the variables.
        self._units = self._moduleRef.getOption("units").split()

        # In case there is a different number of variables and units
        # print a warning. Plots will be made nonetheless.
        if len(self._units) != len(self._variables):
            print "WARNING in HistPlotter: You specified not the same number of units as variables."

        # Get the saveAs string in a form so we can append numbers
        # for multiple plots per module instance...
        saveAsRaw = self._moduleRef.getOption("saveAs")
        if not saveAsRaw:
            raise NameError("ERROR in HistPlotter: Please specify the saveAs option!")
        # pathHead will now contain the path to the folder where
        # we want to save, pathTail only the filename:
        pathHead, pathTail = path.split(saveAsRaw)
        # This loop takes care of filenames with more than one 
        # . in them, for example my.plot.pdf 
        for i, pathTailPart in enumerate(pathTail.split(".")):
            if i == 0:
                self._saveAs = path.join(pathHead, pathTailPart)
            elif i < len(pathTail.split(".")) - 1:
                self._saveAs += pathTailPart
            else:
                self._saveAs += "{filenumber}." + pathTailPart


    def analyse(self, object):
        '''
            Executed on every object:
            Think of this as part of your for-loop which would go over all
            the entries in your text file.
            The actual work is being done here. 
            
            Please ONLY EDIT the code outside the user hooks 
            only IF YOU KNOW WHAT YOU'RE DOING.
        '''

        # The NVectors are saved in so called containers, this converts the object
        #  which is handed from module to module into this original container. 
        container = core.toBasicContainer(object)
        fileCounter = 0

        # This will setup a standard style for all plots and the canvases on which
        # the plots are drawn.
        # Customization will be possible anyway, using the normal pyROOT syntax
        self.setupRoot()

        # If the module is not correctly configured skip the processing to 
        # prevent crashes
        if not self._optionsGood:
            pass

        # This for-loops searches for the variables in the event created
        # by the parser 
        for variableName in self._variables:
            found = False
            for variableSet in container.getObjects():
                # These variables will be used to set the range of the x-axis
                minimum = 0.
                maximum = 0.

                # Check if the variableSet at hand is one containing the values
                # of one of the variables which should be plotted
                if variableSet.getName() == variableName:
                    found = True
                    # This will run over all values and evaluate which are the
                    # minimal and maximal values for the current variable and add
                    # some padding.
                    minimum, maximum = self.determineExtrema(variableSet)

### USER HOOK: Manipulate your histograms here
                    # First lets get the unit of the variable, if one is given:
                    # This gets the index of the current variable. Since units and 
                    # corresponding variables should be in the same order we can use
                    # this index to access the units:
                    iUnit = self._variables.index(variableSet.getName())
                    # Initialize the string which will later be appended to the x-axis
                    # title
                    unitString = ""
                    # Now if we have enough units specified
                    if len(self._units) > iUnit:
                        # And if the unit for the variable is not 0
                        if self._units[iUnit] != "0":
                            # assign the unit with surrounding brackets to the string.
                            # If you want units to be displayed differently just edit
                            # this string.
                            unitString = " (" + self._units[iUnit] + ")"

                    # This initializes a new histogram the arguements are:
                    # 1) the internal name of the histogram
                    # 2) the title of the histogram which will be overwritten
                    # by the initHistStyle below
                    # 3) number of bins, you may want to have different bin-sizes
                    # for different variables this means you may want to have more or
                    # less bins, see the below for this example...
                    # 4 and 5) The minmum  and maximum value of x-axis
                    hist = TH1F ("h_" + variableSet.getName(), "", 100, minimum, maximum)

                    # Now this takes care of the looks of the histogram the arguments should
                    # be self-explainatory.
                    # To change the looks for special variables see below.
                    # For color codes see: http://root.cern.ch/root/html/MACRO_TColor_1_c.gif
                    self.initHistStyle (hist=hist, xTitle=variableSet.getName() + unitString, yTitle="n", histTitle="", color=3, filled=True)

                    # For different bin-sizes uncomment (remove the '#') the following lines and 
                    # change accordingly, also put a # in front of the original hist = ... and 
                    # self.initHistStyle ... lines above
                    #if variableSet.getName() == "the variable you want to change something for":
                    #    # put the according number of bins for nBins:
                    #    hist = TH1F ("h_"+variableSet.getName(), "", nBins, minimum, maximum)
                    #    # Change the looks...
                    #    self.initHistStyle (hist = hist, xTitle = variableSet.getName()+unitString, yTitle = "n", histTitle = "", color = 3, filled = True)
                    #else:
                    #    # For all other variables this style will be used...
                    #    hist = TH1F ("h_"+variableSet.getName(), "", 100, minimum, maximum)
                    #    self.initHistStyle (hist = hist, xTitle = variableSet.getName()+unitString, yTitle = "n", histTitle = "", color = 3, filled = True)


                    # This fills the histogram with the content from the text file / pxlio file
                    for i in range(0, variableSet.getSize()):
                        hist.Fill(variableSet.getElement(i))

                    # Create a canvas
                    c1 = TCanvas()
                    # Draw the histogram on it
                    hist.DrawCopy()
                    # Save the canvas as indicated by the option

                    #find the correct output path
                    self._saveAs = self._saveAs.format(filenumber=fileCounter)
                    if (self._saveAs.find("\\") == -1 and  self._saveAs.find("/") == -1):
                        self._saveAs = self._moduleRef.getAnalysis().getOutputPath() + self._saveAs
                    c1.Print(self._saveAs)
                    # Count the number of output files
                    fileCounter += 1

                    # If you are annoyed by all the pop-up windows containing graphs 
                    # delete this line or better still: put comment in front.
                    c1.WaitPrimitive()

            if not found:
                raise NameError("ERROR in HistPlotter: Sorry, could not find all variables. Make sure they are in the text file: could not find '" + variableName + "'")

### USER HOOK: End.


    def endJob(self):
        '''Executed after the last object'''
        print '*** End job: HistPlotter'

    def determineExtrema(self, variableSet):
        # Initialize
        minimum = 0.
        maximum = 0.
        # run over all values
        for i in range(0, variableSet.getSize()):
            # if smaller or greater than current min or max
            # save values
            if variableSet.getElement(i) < minimum:
                minimum = variableSet.getElement(i)
            if variableSet.getElement(i) > maximum:
                maximum = variableSet.getElement(i)

        # Add a padding:
        if minimum < 0.:
            minimum = minimum + 0.02 * minimum
        maximum = maximum + 0.02 * maximum
        # Return it to the main function
        return minimum, maximum


    def initHistStyle(self, hist, xTitle, yTitle, histTitle, filled=False, color=1):
        # Use the standard style...
        hist.UseCurrentStyle()
        # Self explainatory...
        hist.SetLineColor(color)
        hist.GetXaxis().SetTitle(xTitle)
        hist.GetYaxis().SetTitle(yTitle)
        hist.SetTitle(histTitle)
        if filled:
            hist.SetFillColor(color)
            hist.SetLineColor(1)

    def setupRoot(self):
        gROOT.Reset()   # reset global variables

        # Set ROOT style...
        # If you want to change this check out the
        # Root-manual
        gROOT.SetStyle("Plain")
        gStyle.SetOptStat(0)
        gStyle.SetOptFit(11)
        gStyle.SetStatX(0.9)
        gStyle.SetStatY(0.9)
        gStyle.SetStatW(0.3)
        gStyle.SetStatH(0.2)
        gStyle.SetNdivisions(505, "X")
        gStyle.SetNdivisions(505, "Y")
        gStyle.SetLabelOffset(0.008, "X")
        gStyle.SetLabelSize(0.08, "X")
        gStyle.SetTitleSize(0.08, "X")
        gStyle.SetTitleOffset(1.0, "X")
        gStyle.SetLabelOffset(0.02, "Y")
        gStyle.SetLabelSize(0.08, "Y")
        gStyle.SetTitleSize(0.08, "Y")
        gStyle.SetTitleOffset(1.4, "Y")
        gStyle.SetPadTopMargin(0.15)
        gStyle.SetPadBottomMargin(0.2)
        gStyle.SetPadLeftMargin(0.22)
        gStyle.SetPadRightMargin(0.1)
        gStyle.SetLineWidth(1)
        gStyle.SetMarkerStyle(20)
        gStyle.SetMarkerSize(0.9)
        gStyle.SetMarkerColor(2)
        gStyle.SetCanvasColor(10)
        gStyle.SetFrameFillColor(10)
