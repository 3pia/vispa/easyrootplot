### Skeleton for newestParser.py
# created by VISPA
# Mon Aug 30 20:28:46 2010
### PyGenerate skeleton script

from pxl import modules
import itertools
import sys

class Example(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)

        self._moduleRef = None
        # 'Global' variables
        self._nEvents = 0
        self._inFile = 0
        self._whiteSpaceSeperator = False

        # Options to be added in initialize!
        self._textFileName = ""
        self._parseLines = True
        self._separator = ""
        self._comment = ""
        self._skipLines = 0
        self._maxEntry = 0
        self._titles = True
        self._dataType = float
        pass


    def initialize(self, module):
        ''' Initialize module options '''
        print "--- Init FileParser"

        module.addOption("textFileName", "Name of data input file", "", modules.OptionDescription().USAGE_FILE_OPEN)
        module.addOption("parseLines", "True: A line contains all variables 1 event per row, False: A column contains all variables 1 event per column", True)

        module.addOption("separator", "How are columns seperated in the text file", " ")
        module.addOption("dataType", "Specify the datatype of the entries", "float")
        module.addOption("comment", "What is the exit character for comments", "#")
        module.addOption("skipLines", "Number of Rows to ignore at beginning of file", 0)
        module.addOption("maxEntry", "Last entry to be considered, -1 means consider all. Count begins with first data ntuple.", -1)
        module.addOption("titles", "Wether the first entry is the title of the line/column", True)

        self._moduleRef = module

    def beginJob(self, parameters=None):
        '''Executed before the first object comes in'''

        self._textFileName = self._moduleRef.getOption('textFileName')
        self._textFileName = self._moduleRef.getAnalysis().findFile(self._textFileName)
        print '*** Begin job: File Parser', self._textFileName

        self._parseLines = self._moduleRef.getOption('parseLines')
        self._skipLines = self._moduleRef.getOption('skipLines')
        self._maxEntry = self._moduleRef.getOption('maxEntry')
        self._comment = self._moduleRef.getOption("comment")
        self._separator = self._moduleRef.getOption('separator')
        if self._separator == "":
            print "ERROR - please specify separator! It will be set to ' ' for now."
            self._separator = " "

        if self._separator == " ":
            self._whiteSpaceSeperator = True

        self._titles = self._moduleRef.getOption('titles')
        self._inFile = open(self._textFileName, 'r')

        dataTypeString = self._moduleRef.getOption("dataType")
        if dataTypeString == "float" or dataTypeString == "double":
            self._dataType = float
        if dataTypeString == "int" or dataTypeString == "long":
            self._dataType = int


    def beginRun(self):
        '''Executed before each run'''
        pass

    def generate(self):
        '''Executed on every object'''
        event = core.BasicContainer()

        # Make sure the file is only read once
        # returning None will result in stopping
        # the generator!
        if self._nEvents > 0:
            return None
        # skip the first lines = entries
        for i in xrange(self._skipLines):
            l = self._inFile.readline()

        if not self._parseLines:
            # find out how many columns we have, note:
            # this sets the line pointer to the first
            # line with data...
            firstLine, nCol = self.estimateNumberOfColumns()
            # Now do the actual reading -> puts all columns 
            # as arrays in data
            titles, data = self.readLines(firstLine, nCol)
        else:
            firstLine, nCol = self.estimateNumberOfColumns()
            titles, data = self.readColumns(firstLine, nCol)


        # data is a list of vectors containing the values for a certain variable
        for i, values in enumerate(data):
            valueVector = core.BasicNVector()
            event.insertObject(valueVector)
            valueVector.setSize(len(values))
            if self._titles:
                valueVector.setName(titles[i])
            else:
                valueVector.setName("value" + str(i))

            # convert values to correct type and put into 
            self.setValues(values, valueVector)


        self._nEvents += 1
        return event

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        self._inFile.close()
        print '*** End job: TextFileParser', self._textFileName

    def setValues(self, objects, valueVector):
        # Convert values and put into vector
        positionCounter = 0
        for iEntry, object in enumerate(objects):
            try:
                value = self._dataType(object)
                valueVector.setElement(positionCounter, value)
                positionCounter += 1
            except ValueError:
                print "*** WARNING - Item of wrong type found, continuing with next"
                pass

    def cleanSplit(self, line):
        # get rid of comments and leading / trailing spaces...
        line = line.split(self._comment)[0].strip()
        if line:
            line = line.split(self._separator)
            cleanLine = []
            # Get rid of whitespace in each column
            if self._whiteSpaceSeperator:
                newLine = []
                for lineEle in line:
                    for part in lineEle.split("\t"):
                        newLine.append(part)
                line = newLine
                for object in line:
                    if object != '': cleanLine.append(object)
            else:
                for object in line:
                    cleanLine.append(object.strip())
            return cleanLine
        else:
            return []

    def estimateNumberOfColumns(self):
        # read lines until we find a line with entries
        # estimate number of columns
        firstValues = None
        while not firstValues:
            line = self._inFile.readline()
            #if line = '':
            #    raise IOError("End-of-File reached before finding any data")
            firstValues = self.cleanSplit(line)
            return line, len(firstValues)

    def readLines(self, firstLine, nCol):
        # this temporary list will contain the data
        data = []
        for i in range(nCol):
            data.append([])

        titles = []

        for i, line in enumerate(itertools.chain([firstLine], self._inFile)):
            if self._titles:
                # if we have titles activated store not as data
                if i == 0:
                    titles = self.cleanSplit(line)
                    continue
                # make sure to read only as many events as max entry
                if i == self._maxEntry:
                    break
            elif i == self._maxEntry - 1:
                break

            values = self.cleanSplit(line)

            if values:
                if len(values) == 0:
                    print "*** WARNING - empty line in txt file, continuing with next"
                    continue

                for j, value in enumerate(values):
                    data[j].append(value)
        if len(data[0]) < 2:
            print "WARNING: Found a line with only one value, maybe check your parser."

        return titles, data

    def readColumns(self, firstLine, nCol):
        # this temporary list will contain the data
        data = []
        titles = []

        for i, line in enumerate(itertools.chain([firstLine], self._inFile)):
            lineItems = self.cleanSplit(line)
            if lineItems:
                if self._titles:
                    titles.append(lineItems[0])
                lineItems.pop(0)
                if len(lineItems) == 1:
                    print "WARNING: Found a line with only one value, maybe check your parser."
                data.append(lineItems)

        return titles, data

