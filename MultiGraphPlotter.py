### Skeleton for DataPlotterMultiGraph.py
# created by VISPA
# Fri Sep 17 11:04:14 2010
### PyModule skeleton script

from ROOT import gROOT, gStyle, TGraph, TGraphErrors, TCanvas, gPad, TMath
from os import path
from array import array
from pxl import modules

class Example(modules.PythonModule):
    def __init__(self):
        ''' Initialize private variables '''
        modules.PythonModule.__init__(self)

        self._moduleRef = None

        self._variablePairs = []
        self._foundVariables = {}
        self._errorPairs = []
        self._unitPairs = []
        self._graphs = {}
        self._saveAs = ""
        self._optionsGood = True

    def initialize(self, module):
        ''' Initialize module sinks, sources and options '''
        print "--- Init: MultiGraphPlotter"
#BEGIN USER HOOK: 
        # If you want to combine additional data sources, please
        # add Sinks as shown below. In theory you can add as many
        # as you want.
        module.addSink("input1", "Data Set 1")
        module.addSink("input2", "Data Set 2")
        # The module will analyse all your data streams and split 
        # them again afterwards. To do so you have to add as many
        # sources as sinks, with naming convention:
        # inputx -> outputx, where x is an integer
        module.addSource("output1", "Output port 1")
        module.addSource("output2", "Output port 2")
#END USER HOOK:

        # The variable pairs will be plotted against each other.
        # For example x:y will result in a graph with x vs. y
        module.addOption("variablePairs", "Variables to plot against each other, variables seperated by colon pairs seperated by space e.g., x:y", "x:y x:z")
        # The error pairs have to be denoted as the variables.
        # First error pair for first variable pair, second for 
        # the second variable pair, etc...
        # The first member of a pair will be used as the error
        # for the first member of a variable pair. Write a 0 
        # if a variable has no error.
        module.addOption("errorPairs", "Lines/Columns to use as errors for the variablePairs if no error should be used for one variable write 0 place holder e.g., 'errorx:errory' 'errorx:0' '0:errory'", "errorx:errory errorx:0")

        # Unit pairs are again denoted as the variable pairs
        # and error pairs.
        # If a variable has no unit one can write a 0. The
        # variable will be added as an appendix to the axis 
        # title, e.g. if a variable pair is called x:y and the
        # unit pair is cm:0 it will result in the x-axis being
        # titled "x (cm)" and a y-axis with the title "y"
        module.addOption("unitPairs", "Units of the variables. Denote in same order with same syntax as variablePairs and errorPairs above.", "cm:cm 0:cm")

        # The saveAs option is for the output of this module.
        # Graphs will be saved in the given format and with the
        # given title enumerated in the order as they were put 
        # in the variable pair field.
        # E.g. the saveAs option graphPlot.pdf will result in 
        # plots being saved as pdfs with the titles graphPlot1.pdf,
        # graphPlot2.pdf, etc.
        module.addOption("saveAs", "Specify a format in which the plot should be saved. Possible filetypes: pdf, eps, ps, png, gif, jpg", "graphPlot.pdf", modules.OptionDescription().USAGE_FILE_SAVE)

        self._moduleRef = module

    def beginJob(self, parameters=None):
        '''
            Executed before the first object comes in
            Here some basic initialization is done, i.e.
            loading of options from the GUI
        '''
        print '*** Begin job: MultiGraphPlotter'
        self._optionsGood = True


        # Get variable pairs, which are separated by a whitespace
        varPairsRaw = self._moduleRef.getOption("variablePairs").split()
        # Now split them and fill the pairs into a list
        if not varPairsRaw:
            print "ERROR in GraphPlotter: Please specify variables to draw."
            self._optionsGood = False
        for item in varPairsRaw:
            # For checks if all variables are there create a list of switches
            if len(item.split(":")) != 2:
                raise NameError("ERROR in MultiGraphPlotter: Please always give pairs of variables, e.g. 'x:y t:x'. Do not use quotes.")
            else:
                self._variablePairs.append((item.split(":")[0], item.split(":")[1]))

        # NOTE: Not happy with this, yet. Students will have to write variables
        # and errors in the right order...
        # First get error pairs
        errPairsRaw = self._moduleRef.getOption("errorPairs").split()
        # Now split and fill into list, also appending 0s for variables
        # without errors
        self._errorPairs = self.splitPairOption(errPairsRaw, "errorPairs")
        # Do the same for units:
        unitPairsRaw = self._moduleRef.getOption("unitPairs").split()
        self._unitPairs = self.splitPairOption(unitPairsRaw, "unitPairs")

        # Initialize the list of graphs in which the graphs will
        # be stored per variablePair. And a checklist...
        for xVariableName, yVariableName in self._variablePairs:
            self._graphs[xVariableName + "_" + yVariableName] = {}

        # Get the saveAs string in a form so we can append numbers
        # for multiple plots per module instance...
        saveAsRaw = self._moduleRef.getOption("saveAs")
        if not saveAsRaw:
            raise NameError("ERROR in MultiGraphPlotter: Please specify the saveAs option!")
        # pathHead will now contain the path to the folder where
        # we want to save, pathTail only the filename:
        pathHead, pathTail = path.split(saveAsRaw)
        # This loop takes care of filenames with more than one 
        # . in them, for example my.plot.pdf 
        for i, pathTailPart in enumerate(pathTail.split(".")):
            if i == 0:
                self._saveAs = path.join(pathHead, pathTailPart)
            elif i < len(pathTail.split(".")) - 1:
                self._saveAs += pathTailPart
            else:
                self._saveAs += "{filenumber}." + pathTailPart
        #self._saveAs = self._moduleRef.getAnalysis().findFile(self._saveAs)

    def beginRun(self):
        '''Executed before each run'''
        pass

    def process(self, object, sink):
        '''Executed on every object'''
        # This takes care of the stream splitting, it simply
        # turns inputx to outputx
        source = "output" + sink.split("input")[1]
        currentSinkDescription = ""
        for sinkDescription in self._moduleRef.getSinkDescriptions():
            if sinkDescription.name == sink:
                currentSinkDescription = sinkDescription.description
        container = core.toBasicContainer(object)

        # This takes care of the looks of plots and canvases, which can be
        # customized later nonetheless with normal pyRoot syntax.
        self.setupRoot()
        if not self._optionsGood:
            pass
        # This "for"-loop searches for the variables in the event created
        # by the parser and writes them into arrays which will be used
        # to create the graph later.
        for i, variablePair in enumerate(self._variablePairs):
            xVariableName, yVariableName = variablePair
            xErrorName, yErrorName = self._errorPairs[i]
            xValues = array('f', [])
            xErrors = array('f', [])
            yValues = array('f', [])
            yErrors = array('f', [])
            nEntries = 0
            # Test if we find the variables...
            foundX = False
            foundY = False
            foundEX = False
            foundEY = False
            # Loop over all entries in the event from the txt file...
            for variableSet in container.getObjects():
                currentVariableName = variableSet.getName()
                if currentVariableName == xVariableName:
                    # If the item we look at is the searched xVariable
                    # fill it into the x-array
                    foundX = True
                    for i in range(0, variableSet.getSize()):
                        xValues.append(variableSet.getElement(i))
                        # if there is no error given the error-array has 
                        # to be of same size, so fill it here:
                        if (xErrorName == 0):
                            foundEX = True
                            xErrors.append(0)
                    # need this only for graph constructor
                    nEntries = variableSet.getSize()
                if currentVariableName == xErrorName:
                    # If it is the xError fill into x-error-array...
                    foundEX = True
                    for i in range(0, variableSet.getSize()):
                        xErrors.append(variableSet.getElement(i))

                if currentVariableName == yVariableName:
                    # If it is the yVariable fill into y-array...
                    foundY = True
                    for i in range(0, variableSet.getSize()):
                        yValues.append(variableSet.getElement(i))
                        # if there is no error given the error-array has 
                        # to be of same size, so fill it here:
                        if (yErrorName == 0):
                            foundEY = True
                            yErrors.append(0)

                if currentVariableName == yErrorName:
                    foundEY = True
                    # If it is the yError fill into y-error-array...
                    for i in range(0, variableSet.getSize()):
                        yErrors.append(variableSet.getElement(i))
            if foundEX and foundEY and foundX and foundY:
                graph = TGraphErrors(nEntries, xValues, yValues, xErrors, yErrors)
                # This puts the graph in the list of all graphs, sorted by
                # variables and the input descriptions
                self._graphs[xVariableName + "_" + yVariableName][currentSinkDescription] = graph
            else:
                errorString = "ERROR in MultiGraphPlotter: Sorry, could not find all variables specified make sure they are in the text file:"
                if not foundEX: errorString += " could not find '" + xErrorName + "'"
                if not foundEY: errorString += " could not find '" + yErrorName + "'"
                if not foundX: errorString += " could not find '" + xVariableName + "'"
                if not foundY: errorString += " could not find '" + yVariableName + "'"
                raise NameError(errorString)



        # By returning the source name the event will go
        # through that source
        return source

    def endRun(self):
        '''Executed after each run'''
        pass

    def endJob(self):
        '''Executed after the last object'''
        print '*** End job: MultiGraphPlotter'

        if not self._optionsGood:
            pass

### USER HOOK ----- Here you can customize your plots
        # A list of marker styles and colors for the graphs:
        # For color codes see: http://root.cern.ch/root/html/MACRO_TColor_1_c.gif
        # For marker codes see: http://root.cern.ch/root/html/MACRO_TAttMarker_3_c.gif
        # For line style codes see: http://root.cern.ch/root/html/MACRO_TAttLine_5_Ls.gif
        colors = [1, 2, 4, 6, 7]
        markers = [20, 21, 22, 23, 29]
        lineStyles = [1, 2, 3, 4, 5]
        fileCounter = 0
        for xVariableName, yVariableName in self._variablePairs:
            dataSets = self._graphs[xVariableName + "_" + yVariableName].keys()
            # Lets get the units for the variables. Since we loop over the variable-
            # pairs and count ("enumerate") we can access with the counter of files
            # which corresponds to a counter of the variablePairs:
            # Same again for units:
            xUnit, yUnit = self._unitPairs[fileCounter]

            # Initialize the actual string of variables which will be appended to the
            # axis titles
            xUnitString = ""
            yUnitString = ""
            # If we have a unit which is not 0 save it in the string with brackets 
            # around it
            if xUnit != 0:
                xUnitString = " (" + xUnit + ")"
            if yUnit != 0:
                yUnitString = " (" + yUnit + ")"

            # Create a canvas
            c1 = TCanvas()

            # Get the minimum and maximum to determine the x-axis range
            xMin, xMax, yMin, yMax = self.getExtrema(self._graphs[xVariableName + "_" + yVariableName])
            # Now run over the different inputs, meaning values from different
            # text files.
            for i, dataSet in enumerate(dataSets):
                # The _graphs variable contains all graphs which are initialized
                # in the analyse method. These are sorted by the variables they 
                # show (first key) and also by the txt file from which they are 
                # created (second key)
                graph = self._graphs[xVariableName + "_" + yVariableName][dataSet]
                # This initializes a new graph and takes care of the looks 
                # of it, can be customized with normal pyRoot syntax later.
                self.initGraphStyle(graph=graph, xTitle=xVariableName + xUnitString, yTitle=yVariableName + yUnitString, graphTitle="", xmin=xMin, xmax=xMax, ymin=yMin, ymax=yMax, markerColor=colors[i], marker=markers[i], lineWidth=0, lineColor=colors[i], lineStyle=lineStyles[i])


                # if you want to change the looks of a graph for a certain variable pair you can uncomment (delete the 
                # '#') the following if statement and edit the arguments in the initGraphStyle accordingly:
                # NOTE that if you want to change for example the line-style you'll have to change it in the graph.Draw
                # argument below as well
                #if xVariableName = "put your xVariableName here" and yVariableName == "put your yVariableName here":
                #    if dataSet == "input1 or input2 or whatever you changed it to":
                #        self.initGraphStyle(graph=graph, xTitle = xVariableName+xUnitString, yTitle = yVariableName+yUnitString, graphTitle = "", min = minimum, max = maximum, markerColor = colors[i], marker = markers[i], lineWidth = 0, lineColor = colors[i], lineStyle = lineStyles[i])

                # IMPORTANT NOTE: The character-chain you give the Draw-function
                # specifies what will be drawn, this is unrelated to the options 
                # in the initialization above. Common options are:
                # "A"	Axis are drawn around the graph --> only for first graph!
                # "L"	A simple polyline between every points is drawn
                # "C"	A smooth Curve is drawn
                # "*"	A Star is plotted at each point
                # "P"	Idem with the current marker
                # for more check: http://root.cern.ch/root/html/TGraphPainter.html
                if i == 0:
                    graph.Draw("APL")
                else:
                    graph.Draw("PL")

            #find the correct output path
            self._saveAs = self._saveAs.format(filenumber=fileCounter)
            if (self._saveAs.find("\\") == -1 and  self._saveAs.find("/") == -1):
                self._saveAs = self._moduleRef.getAnalysis().getOutputPath() + self._saveAs

            # Print the canvas    
            c1.Print(self._saveAs)
            # Count the files
            fileCounter += 1
            # Wait for input, delete this if the pop up annoys you.
            c1.WaitPrimitive()

### END USER HOOK

    def initGraphStyle(self, graph, xTitle, yTitle, graphTitle, xmin, xmax, ymin, ymax, markerColor=1, marker=20, lineWidth=0, lineColor=1, lineStyle=1):
        graph.GetXaxis().SetTitle(xTitle)
        graph.GetYaxis().SetTitle(yTitle)
        graph.SetTitle(graphTitle)
        graph.GetYaxis().SetRangeUser(ymin, ymax)
        graph.GetXaxis().SetLimits(xmin, xmax)
        graph.UseCurrentStyle()
        graph.SetLineWidth(lineWidth)
        graph.SetLineColor(lineColor)
        graph.SetMarkerColor(markerColor)
        graph.SetMarkerStyle(marker)
        graph.SetLineStyle(lineStyle)

    def getExtrema(self, graphs):
        xmin = 0
        xmax = 0
        ymin = 0
        ymax = 0

        searchedMinimum = False

        for set, graph in graphs.iteritems():
            xVals = graph.GetX()
            xErrs = graph.GetEX()
            yVals = graph.GetY()
            yErrs = graph.GetEY()
            if xmin == 0 and not searchedMinimum:
                xmin = xVals[0]
                ymin = yVals[0]
                searchedMinimum = True

            for i in range(graph.GetN()):
                if xVals[i] - xErrs[i] < xmin:
                    if gPad and gPad.GetLogx():
                        if xErrs[i] < xVals[i]: xmin = xVals[i] - xErrs[i]
                        else: xmin = min(xmin, xVals[i] / 3)
                    else:
                        xmin = xVals[i] - xErrs[i]
                if xVals[i] + xErrs[i] > xmax:
                    xmax = xVals[i] + xErrs[i]

                if yVals[i] - yErrs[i] < ymin:
                    if gPad and gPad.GetLogy():
                        if yErrs[i] < yVals[i]: ymin = yVals[i] - yErrs[i]
                        else: ymin = min(ymin, yVals[i] / 3)
                    else:
                        ymin = yVals[i] - yErrs[i]
                if yVals[i] + yErrs[i] > ymax:
                    ymax = yVals[i] + yErrs[i]
        # apply padding:
        if xmin > 0:
            xmin *= 0.95
        else:
            xmin *= 1.05
        if xmax > 0:
            xmax *= 1.05
        else:
            xmax *= 0.95

        if ymin > 0:
            ymin *= 0.95
        else:
            ymin *= 1.05
        if ymax > 0:
            ymax *= 1.05
        else:
            ymax *= 0.95

        return xmin, xmax, ymin, ymax

    def setupRoot(self):
        gROOT.Reset()   # reset global variables

        # Set ROOT style...
        # If you want to change this check out the
        # Root-manual
        gROOT.SetStyle("Plain")
        gStyle.SetOptStat(0)
        gStyle.SetOptFit(11)
        gStyle.SetStatX(0.9)
        gStyle.SetStatY(0.9)
        gStyle.SetStatW(0.3)
        gStyle.SetStatH(0.2)
        gStyle.SetNdivisions(505, "X")
        gStyle.SetNdivisions(505, "Y")
        gStyle.SetLabelOffset(0.008, "X")
        gStyle.SetLabelSize(0.08, "X")
        gStyle.SetTitleSize(0.08, "X")
        gStyle.SetTitleOffset(1.0, "X")
        gStyle.SetLabelOffset(0.02, "Y")
        gStyle.SetLabelSize(0.08, "Y")
        gStyle.SetTitleSize(0.08, "Y")
        gStyle.SetTitleOffset(1.4, "Y")
        gStyle.SetPadTopMargin(0.15)
        gStyle.SetPadBottomMargin(0.2)
        gStyle.SetPadLeftMargin(0.22)
        gStyle.SetPadRightMargin(0.1)
        gStyle.SetLineWidth(1)
        gStyle.SetMarkerStyle(20)
        gStyle.SetMarkerSize(0.9)
        gStyle.SetMarkerColor(2)
        gStyle.SetCanvasColor(10)
        gStyle.SetFrameFillColor(10)

    def splitPairOption(self, rawPairs, option):
        splittedPairOption = []
        if len(rawPairs) != 0:
            for item in rawPairs:
                if len(item.split(":")) != 2:
                    print "ERROR in GraphPlotter: Please always give pairs in", option, "as in 'x:y t:x'. Do not use quotes."
                    self._optionsGood = False
                else:
                    xVal = item.split(":")[0]
                    yVal = item.split(":")[1]
                    if xVal == "0": xVal = float(xVal)
                    if yVal == "0": yVal = float(yVal)
                    splittedPairOption.append((xVal, yVal))

        # If less units than variables are given fill with 0s...
        if len(splittedPairOption) < len(self._variablePairs):
            print "WARNING: Please give as many", option, "as variablePairs. Filling the rest with 0s."
            nDefPairs = len(splittedPairOption)
            for i in range(len(self._variablePairs) - nDefPairs):
                splittedPairOption.append((0, 0))

        return splittedPairOption


